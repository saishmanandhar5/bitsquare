import React from 'react'
import '../css/Navbar.css'
import Logo from '../images/Logo.png'
import '../css/common.css'



export default function Navbar(props) {
  
  return (
    <>
    <div className="container flex-between align-center" style={{
      paddingTop:".3rem"
    }}>
        <img src={Logo}alt="logo" className='logo' />
        <div className="languagechange">

        <button onClick={props.language('en')}>English</button>
        <button onClick={props.language('it')}>Italian</button>
        </div>
          <button className='subscribebtn' >Subscribe</button>
          </div>
          </>
        )
      }
      
        

