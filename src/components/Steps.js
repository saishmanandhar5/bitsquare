import React from 'react'
import '../css/Page3.css'

import '../css/common.css'
import Image3 from '../images/image3.png'
import Mobile from '../images/Rectangle.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClock } from '@fortawesome/free-solid-svg-icons'
import { faMoneyBills } from '@fortawesome/free-solid-svg-icons'
import { faDollarSign } from '@fortawesome/free-solid-svg-icons'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { faCalendarDays } from '@fortawesome/free-solid-svg-icons'

import { faBell } from '@fortawesome/free-solid-svg-icons'

export default function Steps() {
    return (
        <>
            <div className="content3">
                <h1 className='text-heading'>Steps to build your square</h1>
                <div className="image3 flex-center">
                    <img src={Image3}  className="steps" />
                </div>
            </div>
            <div className="content4 flex-between flex-center">

                <div className="mobile">
                    <img src={Mobile} className='mobile-photo' style={{
                    
                    }}/>
                </div>
                <div className='connect-bitsquare'>
                    <div style={{
                        paddingBottom:"2rem"
                    }}> 
                        <h1 className='text-heading steps-title'> Why connect with BitSquare</h1>
                    </div>
                   <div className="bitsqrs-customers">

                    <div className='bitsqrs flex-column align-center'>
                            
                        <h3 className='text-heading'style={{display:'inline'}}>BitSqrs</h3>
                        <div className='flex align-center'>
                        <FontAwesomeIcon icon={faClock} color='#153D5F'className='font-icons' /><p className='text-bitsqr'><strong>Organize online & in person session</strong></p>
                        </div>
                        <div className="flex align-center">

                        <FontAwesomeIcon icon={faCalendarDays} className='font-icons' color='#153D5F' /><p className='text-bitsqr'><strong>Plan your smart and   digital agenda</strong></p>
                        </div>
                        <div className="flex align-center">

                        <FontAwesomeIcon icon={faMoneyBills} color='#153D5F'className='font-icons' /> <p className='text-bitsqr'><strong>Receive secure  in-app payments</strong></p>
                        </div>
                        
                    </div>
                        <div className='customers flex-column align-center'>
                            <h3 className='text-heading'style={{display:'inline'}} >Customers & Followers</h3>
                            <div className="flex align-center">
                        <FontAwesomeIcon icon={faDollarSign} color='#153D5F'className='font-icons' /><p className='text-user'><strong> Price for all</strong></p>
                            </div>
                        <div className="flex align-center">
                        <FontAwesomeIcon icon={faSearch} color='#153D5F'className='font-icons' /><p className='text-user'><strong>Search for the best Bitsqrs</strong></p>
                        </div>
                        <div className="flex align-center">
                        <FontAwesomeIcon icon={faBell} color='#153D5F' className='font-icons'/><p className='text-user' ><strong>Receive remainders</strong></p>
                        </div>
                        </div>
                   </div>
                </div>
            </div>

        </>
    )
}

                            
                            
