import React from 'react'
import Image1 from '../images/Union.png'
import '../css/Page1.css'
import '../css/common.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAddressBook } from '@fortawesome/free-solid-svg-icons'
import { faUserGroup } from '@fortawesome/free-solid-svg-icons'
import { faAnglesDown } from '@fortawesome/free-solid-svg-icons'

export default function YourSquare(props) {
  return (
    <>
      <div className="content1 flex-center ">

        <img src={Image1} alt="image1" className='image1'  style={{
          marginTop:"6rem"
        }}/>
     
       

    

     
        
        <div className="text-head">
          <h1 className='text-heading '>{props.t('build-square')}</h1>
          <p className='text-body'>{props.t('bitsquareistheapp')}<br />
          {props.t('bitsquarerscustomers')}    </p>
        
      
        <div className="text-icons flex-around ">
          <div className="address flex-column">
            <FontAwesomeIcon icon={faAddressBook} size="4x" className='fontawesome' color='#153D5F'/>
            <h2 className='text-heading' >BitSquarer</h2>
            <p className='text-body'>
            {props.t('anyone')} <br />
            {props.t('monetize')}
            </p>
          </div>
          <div className="followers flex-column"
          style={{
            paddingLeft:"12rem"
          }}
          ></div>
            <FontAwesomeIcon icon={faUserGroup} className='fontawesome' size="4x" color='#153D5F' />
            <h2 className='text-heading'>Users</h2>
            <p className='text-body'>
            {props.t('choosesession')} <br />
            {props.t('agenda')}
            </p>
            </div>
          </div>
          
          
          
        </div>
        <div className="text">
            <p className='text-body central-text'>{props.t('session')} <br />
            <strong>{props.t('online')}</strong> {props.t('or')} <strong>{props.t('inperson')}</strong></p>
          </div>
          <div className="icon flex-center">
            <FontAwesomeIcon icon={faAnglesDown} className='fontawesome' size="4x" color='#153D5F' />

          </div>



    </>

  )
}
