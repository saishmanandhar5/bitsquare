import React from 'react'
import '../css/Page2.css'
import '../css/common.css'
import Image2 from '../images/image2.png'
import Image2it from '../images/image2it.png'
import Bitsquare from '../images/BitSquare.png'

export default function YourProfession(props) {
  return (
    <>
    <div className="content2 ">
    lang{props.t()}
        <h1 className='text-heading'>{props.t('whateverprofession')}</h1>
        <div className="image flex-center">
          
        <img src={props.language('en')? Image2 : Image2it} alt="image2" className='image2' />
        </div>
        <div className='bitsquare-text flex-center' style={{
          padding:"4rem 0"
        }}>
            <img src= {Bitsquare} className='bitsquare'/> <strong className='text-body'>is the right square for you!</strong> 
        </div>
    </div>
    </>
  )
}
