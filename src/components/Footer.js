import React from 'react'
import '../css/Page4.css'
import '../css/common.css'

export default function Footer() {
  return (
    <>
    <div className="footer flex-around footer-text">
        <div className="left">
            <p>© 2022 BitSquare. All Rights reserved.</p>
        </div>
        <div className="right footer-text">
        <p>Reach us at : info@bitsquare.app</p>
        </div>
    </div>
    </>
  )
}
