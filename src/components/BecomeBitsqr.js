import React from 'react'
import '../css/Page4.css'
import '../css/common.css'

export default function BecomeBitsqr(props) {
  return (
    <>
    <section style={{width:'100%'}} className=' content5 flex-center align-center'>
    <div className=" becomeBit flex-column flex-center align-center">
        <div className='text-content'>
            <h1 className='c5-heading1'>{props.t('build-square')}</h1>
            <p className='c5-heading2'>Enter your email below and get on the waiting list</p>
        </div>
        <div className="input-field flex-center align-center">
        <div className="text-area ">
            <input type="text" className='email' placeholder='subscribe'/>
        </div>
            <div className="subbutton">

            <button className='subscribebtn2' >Subscribe</button>
            </div>
            </div>
    </div>
    </section>
    
   
    </>
  )
}
