
import './App.css';
import BecomeBitsqr from './components/BecomeBitsqr';
import Footer from './components/Footer';
import Navbar from './components/Navbar';
import Steps from './components/Steps';
import YourProfession from './components/YourProfession';
import YourSquare from './components/YourSquare';
import i18n from "i18next";
import { useTranslation, initReactI18next} from "react-i18next";
import TEn from './components/en.json'
import TIt from './components/it.json'


i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    // the translations
    // (tip move them in a JSON file and import them,
    // or even better, manage them via a UI: https://react.i18next.com/guides/multiple-translation-files#manage-your-translations-with-a-management-gui)
    resources: {
      en: {
        translation: TEn
      },
       it : {
        translation: TIt
      }
    },
    lng: "en", // if you're using a language detector, do not define the lng option
    fallbackLng: "en",

    interpolation: {
      escapeValue: false // react already safes from xss => https://www.i18next.com/translation-function/interpolation#unescape
    }
  });


function App() {
  const { t }= useTranslation(); 

  const changlang =(l)=>{
    return()=>{
      i18n.changeLanguage(l);
    
    }
  }

  return (  
    <>
    <div className="main-container">

   <Navbar t ={ t } language ={changlang}/>
  <YourSquare t ={ t } language ={changlang}/>
  <YourProfession t ={ t} language ={changlang}/>
  <Steps t ={ t()}/>
  <BecomeBitsqr t ={ t } language ={changlang}/>
    </div>
  <Footer t ={ t}/>  
  </>
  );
}


export default App;

   


